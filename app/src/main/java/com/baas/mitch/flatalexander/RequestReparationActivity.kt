package com.baas.mitch.flatalexander

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.*

var backPressCount = 0

class RequestReparationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_reparation)
    }

    override fun onBackPressed() {
        val problemDescriptionFilled = findViewById<EditText>(R.id.editText).text.isNotEmpty()
        if(problemDescriptionFilled && backPressCount == 0)    {
            backPressCount += 1
            Toast.makeText(this, getString(R.string.request_reparation_toast_text_leave_before_send), Toast.LENGTH_LONG).show()
        } else {
            backPressCount = 0
            super.onBackPressed()
        }

    }

    fun sendDataToMailClientAndStart(view: View) {
        val urgencyRadioGroup = findViewById<RadioGroup>(R.id.radioGroup)
        val enterUnitRadioGroup = findViewById<RadioGroup>(R.id.radioGroup2)
        val urgencyCheckedRadioButton = findViewById<RadioButton>(urgencyRadioGroup.checkedRadioButtonId).text.toString()
        val problemDescription = findViewById<EditText>(R.id.editText).text.toString()
        val enterUnitCheckedRadioButton = findViewById<RadioButton>(enterUnitRadioGroup.checkedRadioButtonId).text.toString()
        val unitNumber = findViewById<EditText>(R.id.editText2).text.toString()

        if(problemDescription.isNotEmpty() && unitNumber.isNotEmpty())    {
            val emailBody =
                    "${getString(R.string.request_reperation_unit_number)}: $unitNumber\n\n" +
                    "${getString(R.string.request_reperation_description)}: $problemDescription\n\n" +
                    "${getString(R.string.request_reparation_text_enter_unit)} $enterUnitCheckedRadioButton"

            // setting up the mail sendto format
            // cc MUST be included otherwise it dies.
            val mailTo = "mailto:woco.test.acc@gmail.com" +
                    "?cc=" +
                    "&subject=Reparatieverzoek, Unit: $unitNumber, Urgentie: " + Uri.encode(urgencyCheckedRadioButton) +
                    "&body=" + Uri.encode(emailBody)
            val test = Intent(Intent.ACTION_SENDTO)
            test.setData(Uri.parse(mailTo))
            startActivity(test)
        } else  {
            Toast.makeText(this, getString(R.string.request_reparation_toast_text_empty_description), Toast.LENGTH_LONG).show()
        }
    }
}
