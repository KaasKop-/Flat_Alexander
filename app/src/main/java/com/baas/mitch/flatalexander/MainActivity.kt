package com.baas.mitch.flatalexander

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.content.Intent

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun startReparationRequest(view: View) {
        val intent = Intent(this, RequestReparationActivity::class.java)
        startActivity(intent)
    }

    fun startWashingDryingActivity(view: View)  {
        val intent = Intent(this, WashingActivity::class.java)
        startActivity(intent)
    }
}
