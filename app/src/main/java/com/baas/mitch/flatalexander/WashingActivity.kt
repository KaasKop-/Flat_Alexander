package com.baas.mitch.flatalexander

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView


class WashingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_washing)
        val webView = findViewById<WebView>(R.id.webView1)
        webView.loadUrl("http://flatalexander.nl/booked/Web/index.php")
    }
}
